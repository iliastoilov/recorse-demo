﻿using System;
using Android.Content;
using Android.Graphics.Drawables;
using RecorseDemo.Droid.Renderers;
using RecorseDemo.UI;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(CustomDatePickerRenderer))]

namespace RecorseDemo.Droid.Renderers
{
    public class CustomDatePickerRenderer : DatePickerRenderer
    {
        public CustomDatePickerRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.DatePicker> e)
        {
            base.OnElementChanged(e);

            GradientDrawable gradient = new GradientDrawable(GradientDrawable.Orientation.TopBottom, new[] {
                    Colors.ControlsBackgroundColor.ToAndroid(),
                    Colors.ControlsBackgroundColor.ToAndroid().ToArgb()});

            gradient.SetCornerRadius(15);
            gradient.SetStroke(2, Colors.AccentColor.ToAndroid());

            Control.SetBackground(gradient);

            //this.Control.SetPadding(20, 10, 20, 10);
        }
    }
}