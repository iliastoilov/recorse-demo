﻿using System;
using System.Collections.Generic;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace RecorseDemo.Logger
{
    public class AppLogger
    {
        public void Init()
        {
            AppCenter.Start(AppSettings.AppCenterKey, typeof(Analytics), typeof(Crashes));
            AppCenter.LogLevel = LogLevel.Error;
        }

        public void Report(LogType logType, string message, Exception ex = null)
        {
            var properties = new Dictionary<string, string>();

            var log = FormatLog(logType, message, ex);

            Console.WriteLine(log);
            properties.Add("Message", message);
            Crashes.TrackError(ex, properties);
        }

        private string FormatLog(LogType logType, string message, Exception ex = null)
        {
            string result = DateTime.UtcNow.ToString("dd/MM/yyyy HH:mm:ss");

            switch (logType)
            {
                case LogType.Info:
                    result += " INFO: ";
                    break;
                case LogType.Warning:
                    result += " WARNING: ";
                    break;
                case LogType.Error:
                    result += " ERROR ";
                    break;
                case LogType.Fatal:
                    result += " FATAL: ";
                    break;
                case LogType.Debug:
                    result += " DEBUG: ";
                    break;
            }

            result += message;
            result += " " + ex?.ToString() + " " + ex?.Message + " " + ex?.StackTrace + "\n";

            return result;
        }
    }

    public enum LogType
    {
        Info = 0,
        Warning = 1,
        Error = 2,
        Fatal = 3,
        Debug = 4,
    }
}
