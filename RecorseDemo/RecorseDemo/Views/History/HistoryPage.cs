﻿using System;
using RecorseDemo.UI;
using RecorseDemo.UI.BasicControls;
using Xamarin.Forms;

namespace RecorseDemo.Views.History
{
    public class HistoryPage : BasePage
    {
        public HistoryPage()
        {
            BindingContext = new HistoryPageModel();

            #region Controls
            var btnNew = new CustomImageButton(Images.NewIcon);
            btnNew.HeightRequest = 30;

            var imgLogo = new CustomImage(Images.Logo);

            var listNotes = new CustomListView(ListViewCachingStrategy.RecycleElementAndDataTemplate);
            listNotes.BackgroundColor = Colors.MainColor;
            listNotes.ItemTemplate = new DataTemplate(typeof(NoteCell));
            listNotes.HasUnevenRows = true;
            listNotes.SeparatorVisibility = SeparatorVisibility.None;
            listNotes.SelectionMode = ListViewSelectionMode.None;
            #endregion

            #region Layout
            var grid = new CustomGrid();
            grid.Padding = 10;
            grid.RowSpacing = 10;
            grid.AddRows(2, 1, GridUnitType.Auto);

            grid.Add(imgLogo, 0, 1, 0, 1);
            grid.Add(btnNew, 0, 1, 0, 1);
            grid.Add(listNotes, 0, 1, 1, 2);

            Content = grid;
            #endregion

            #region Bindings
            listNotes.SetBinding(ListView.ItemsSourceProperty, nameof(HistoryPageModel.Notes));
            btnNew.SetCommandBinding(nameof(HistoryPageModel.NewCommand));
            #endregion
        }
    }
}