﻿using System;
using RecorseDemo.LocalDatabase.Models;
using RecorseDemo.UI;
using RecorseDemo.Views.Manage;
using Xamarin.Forms;

namespace RecorseDemo.Views.History
{
    public class NoteCell : CustomViewCell
    {
        public NoteCell()
        {
            var lblDate = new CustomLabel();
            lblDate.FontAttributes = FontAttributes.Bold;

            var lblNoteText = new CustomLabel();

            var stack = new CustomStackLayout()
            {
                Children =
                {
                    lblDate,
                    lblNoteText,
                }
            };

            View = new CustomFrame(stack);

            #region Bindings
            lblDate.SetTextBinding(nameof(Note.Date), format: "{0:" + AppSettings.DateFormat + "}");
            lblNoteText.SetTextBinding(nameof(Note.Text));
            #endregion

            #region Gestures
            var tap = new TapGestureRecognizer();
            tap.Command = new Command(NoteSelected);
            View.GestureRecognizers.Add(tap);
            #endregion
        }

        private void NoteSelected()
        {
            if (BindingContext is Note note)
            {
                App.Current.MainPage = new ManagePage(note);
            }
        }
    }
}
