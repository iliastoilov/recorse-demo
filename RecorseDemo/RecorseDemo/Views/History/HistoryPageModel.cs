﻿using System;
using System.Collections.ObjectModel;
using RecorseDemo.LocalDatabase.Models;
using RecorseDemo.LocalDatabase.Services;
using RecorseDemo.UI;
using RecorseDemo.Views.Manage;
using Xamarin.Forms;

namespace RecorseDemo.Views.History
{
    public class HistoryPageModel : BaseViewModel
    {
        public ObservableCollection<Note> Notes { get; set; }

        public Command NewCommand { get; private set; }

        public HistoryPageModel()
        {
            NewCommand = new Command(New);
            Init();
        }

        private async void Init()
        {
            App.IsBusy = true;

            var noteService = new NoteService();
            Notes = new ObservableCollection<Note>(await noteService.GetAllDataAsync());

            App.IsBusy = false;
        }

        private void New()
        {
            App.Current.MainPage = new ManagePage();
        }
    }
}
