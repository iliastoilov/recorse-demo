﻿using System;
using RecorseDemo.LocalDatabase.Models;
using RecorseDemo.UI;
using RecorseDemo.UI.BasicControls;
using Xamarin.Forms;

namespace RecorseDemo.Views.Manage
{
    public class ManagePage : BasePage
    {
        public ManagePage(Note note = null)
        {
            BindingContext = new ManagePageModel(note);

            #region Controls
            var btnBack = new CustomImageButton(Images.BackIcon);
            btnBack.HeightRequest = 35;

            var imgLogo = new CustomImage(Images.Logo);

            var lblNote = new CustomLabel(AppTexts.Text);
            var txtNote = new CustomEditor();
            txtNote.VerticalOptions = LayoutOptions.FillAndExpand;
            txtNote.AutoSize = EditorAutoSizeOption.Disabled;

            var lblDate = new CustomLabel(AppTexts.Date);
            var Date = new CustomDatePicker();

            var btnRemove = new CustomButton(AppTexts.Remove);
            btnRemove.BorderColor = Color.Red;
            btnRemove.TextColor = Color.Red;

            var btnSave = new CustomButton(AppTexts.Save);
            #endregion

            #region Layout
            var grid = new CustomGrid();
            grid.Padding = 10;
            grid.RowSpacing = 10;
            grid.AddRows(4, 1, GridUnitType.Auto);
            grid.AddRow(1, GridUnitType.Star);
            grid.AddRows(2, 1, GridUnitType.Auto);

            grid.Add(imgLogo, 0, 1, 0, 1);
            grid.Add(btnBack, 0, 1, 0, 1);
            grid.Add(lblDate, 0, 1, 1, 2);
            grid.Add(Date, 0, 1, 2, 3);
            grid.Add(lblNote, 0, 1, 3, 4);
            grid.Add(txtNote, 0, 1, 4, 5);
            grid.Add(btnSave, 0, 1, 5, 6);
            grid.Add(btnRemove, 0, 1, 6, 7);

            Content = grid;
            #endregion

            #region Bindings
            txtNote.SetTextBinding(nameof(ManagePageModel.Text));
            Date.SetBinding(DatePicker.DateProperty, nameof(ManagePageModel.Date));
            btnSave.SetCommandBinding(nameof(ManagePageModel.SaveCommand));
            btnBack.SetCommandBinding(nameof(ManagePageModel.BackCommand));
            btnRemove.SetCommandBinding(nameof(ManagePageModel.RemoveCommand));
            btnRemove.SetBinding(Button.IsVisibleProperty, nameof(ManagePageModel.EditMode));
            #endregion
        }
    }
}
