﻿using System;
using RecorseDemo.LocalDatabase.Models;
using RecorseDemo.LocalDatabase.Services;
using RecorseDemo.UI;
using RecorseDemo.Views.History;
using Xamarin.Forms;

namespace RecorseDemo.Views.Manage
{
    public class ManagePageModel : BaseViewModel
    {
        public Guid ID { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public bool EditMode { get; set; }

        public Command SaveCommand { get; set; }
        public Command RemoveCommand { get; set; }
        public Command BackCommand { get; set; }

        public ManagePageModel(Note note = null)
        {
            Date = DateTime.Now;

            if (note != null)
            {
                ID = note.ID;
                Text = note.Text;
                Date = note.Date;

                SaveCommand = new Command(SaveChanges);
                RemoveCommand = new Command(Remove);
                EditMode = true;
            }
            else
            {
                SaveCommand = new Command(Save);
            }

            BackCommand = new Command(Back);
        }

        private void Back()
        {
            App.Current.MainPage = new HistoryPage();
        }

        private async void Save()
        {
            if (string.IsNullOrWhiteSpace(Text))
            {
                await ShowMessage(AppTexts.Alert, AppTexts.PlsInsertText, AppTexts.Ok);
                return;
            }

            App.IsBusy = true;

            var noteService = new NoteService();
            await noteService.AddAsync(new Note()
            {
                ID = Guid.NewGuid(),
                Text = Text,
                Date = Date,
            });

            App.IsBusy = false;

            Back();
        }

        private async void SaveChanges()
        {
            if (string.IsNullOrWhiteSpace(Text))
            {
                await ShowMessage(AppTexts.Alert, AppTexts.PlsInsertText, AppTexts.Ok);
                return;
            }

            App.IsBusy = true;

            var noteService = new NoteService();
            await noteService.UpdateAsync(new Note()
            {
                ID = ID,
                Text = Text,
                Date = Date,
            });

            App.IsBusy = false;

            Back();
        }

        private async void Remove()
        {
            App.IsBusy = true;

            var noteService = new NoteService();
            await noteService.RemoveAsync(new Note()
            {
                ID = ID,
                Text = Text,
                Date = Date,
            });

            App.IsBusy = false;

            Back();
        }
    }
}
