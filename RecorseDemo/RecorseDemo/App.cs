﻿using System;
using Acr.UserDialogs;
using RecorseDemo.Logger;
using RecorseDemo.Views.History;
using RecorseDemo.Views.Manage;
using Xamarin.Forms;

namespace RecorseDemo
{
    public class App : Application
    {
        public static AppLogger Logger = new AppLogger();

        public App()
        {
            MainPage = new HistoryPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        #region IsBusy implementation
        private static bool isBusy = false;

        public static bool IsBusy
        {
            get { return isBusy; }
            set
            {
                isBusy = value;
                HandleLoading();
            }
        }

        public static void HandleLoading()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (IsBusy)
                {
                    UserDialogs.Instance.Loading().Show();
                }
                else
                {
                    UserDialogs.Instance.Loading().Hide();
                }
            });
        }
        #endregion
    }
}
