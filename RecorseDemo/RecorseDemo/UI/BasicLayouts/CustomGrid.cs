﻿using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public class CustomGrid : Grid
    {
        public CustomGrid()
        {
            this.Padding = new Thickness(0);
            this.Margin = new Thickness(0);
        }

        public void AddColumn(double width, GridUnitType type = GridUnitType.Absolute)
        {
            this.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(width, type) });
        }

        public void AddColumns(int count, double width, GridUnitType type = GridUnitType.Absolute)
        {
            for (int i = 0; i < count; i++)
            {
                this.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(width, type) });
            }
        }

        public void AddRow(double height, GridUnitType type = GridUnitType.Absolute)
        {
            this.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(height, type) });
        }

        public void AddRows(int count, double height, GridUnitType type = GridUnitType.Absolute)
        {
            for (int i = 0; i < count; i++)
            {
                this.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(height, type) });
            }
        }

        public void Add(View view, int left, int right, int top, int bottom)
        {
            this.Children.Add(view, left, right, top, bottom);
        }

        public void AddItemsInColumn(View[] views, int left, int right, int startRow)
        {
            int nextTop = startRow;
            foreach (var item in views)
            {
                int nextBottom = nextTop + 1;
                Children.Add(item, left, right, nextTop, nextBottom);
                nextTop++;
            }
        }

        public void AddItemsInRow(View[] views, int top, int bottom, int startColumn)
        {
            int nextLeft = startColumn;
            foreach (var item in views)
            {
                int nextRight = nextLeft + 1;
                Children.Add(item, nextLeft, nextRight, top, bottom);
                nextLeft++;
            }
        }
    }
}