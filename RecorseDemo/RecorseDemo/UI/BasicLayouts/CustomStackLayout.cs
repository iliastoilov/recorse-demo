﻿using System;
using System.Collections;
using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public class CustomStackLayout : StackLayout
    {
        public static readonly BindableProperty ItemSourceProperty =
  BindableProperty.Create(nameof(ItemSource), typeof(IEnumerable), typeof(IEnumerable), null, propertyChanged: ItemSourceChanged);

        public IEnumerable ItemSource
        {
            get { return (IEnumerable)base.GetValue(ItemSourceProperty); }
            set { base.SetValue(ItemSourceProperty, value); }
        }

        private static void ItemSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            BindableLayout.SetItemsSource((StackLayout)bindable, (IEnumerable)newValue);
        }

        public DataTemplate ItemTemplate
        {
            set
            {
                BindableLayout.SetItemTemplate(this, value);
            }
        }

        public void Add(View view)
        {
            this.Children.Add(view);
        }
    }
}
