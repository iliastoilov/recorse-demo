﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace RecorseDemo.UI
{
    public class BasePage : ContentPage
    {
        public BasePage()
        {
            On<iOS>().SetUseSafeArea(true);
            BackgroundColor = Colors.MainColor;
        }

        //protected override void OnAppearing()
        //{
        //    var safeInsets = On<iOS>().SafeAreaInsets();
        //    safeInsets.Bottom = 0;
        //    Padding = safeInsets;
        //    base.OnAppearing();
        //}
    }
}