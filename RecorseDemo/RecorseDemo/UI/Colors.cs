﻿using System;
using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public static class Colors
    {
        public static Color MainColor = Color.FromRgb(36, 42, 54);
        public static Color ControlsBackgroundColor = Color.FromRgb(48, 56, 74); 
        public static Color AccentColor = Color.White;
    }
}
