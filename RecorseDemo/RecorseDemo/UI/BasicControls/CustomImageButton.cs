﻿using System;
using Xamarin.Forms;

namespace RecorseDemo.UI.BasicControls
{
    public class CustomImageButton : ImageButton
    {
        public CustomImageButton(string imageSource = "")
        {
            Source = imageSource;
            HorizontalOptions = LayoutOptions.Start;
            VerticalOptions = LayoutOptions.Start;
            BackgroundColor = Color.Transparent;
            Margin = 0;
            Padding = 0;
        }

        public void SetCommandBinding(string propertyName)
        {
            this.SetBinding(Button.CommandProperty, propertyName);
        }

        public void SetCommandParameterBinding(string propertyName)
        {
            this.SetBinding(Button.CommandParameterProperty, propertyName);
        }
    }
}
