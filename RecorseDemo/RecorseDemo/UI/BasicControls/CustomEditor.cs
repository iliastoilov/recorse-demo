﻿using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public class CustomEditor : Editor
    {
        public CustomEditor()
        {
            TextColor = Colors.AccentColor;
        }

        public void SetTextBinding(string propertyName)
        {
            this.SetBinding(Editor.TextProperty, propertyName);
        }
    }
}

