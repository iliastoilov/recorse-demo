﻿using System;
using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public class CustomImage : Image
    {
        public CustomImage(string imageSource = "")
        {
            Source = imageSource;
        }
    }
}
