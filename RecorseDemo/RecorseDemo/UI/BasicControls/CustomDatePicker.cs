﻿using System;
using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public class CustomDatePicker : DatePicker
    {
        public CustomDatePicker()
        {
            Format = AppSettings.DateFormat;
            TextColor = Colors.AccentColor;
            BackgroundColor = Color.Transparent;
        }
    }
}