﻿using System;
using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public class CustomLabel : Label
    {
        public CustomLabel(string text = "")
        {
            Text = text;
            TextColor = Colors.AccentColor;
            VerticalOptions = LayoutOptions.Center;
            HorizontalOptions = LayoutOptions.Start;
            HorizontalTextAlignment = TextAlignment.Start;
        }

        public void SetTextBinding(string propertyName, IValueConverter valueConverter = null, string format = "{0}")
        {
			this.SetBinding(Label.TextProperty, propertyName, converter: valueConverter, stringFormat: format);
        }
	}
}
