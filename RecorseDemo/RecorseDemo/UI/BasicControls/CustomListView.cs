﻿using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace RecorseDemo.UI
{
    public class CustomListView : Xamarin.Forms.ListView
	{
        public CustomListView(ListViewCachingStrategy cachingStrategy) : base(cachingStrategy)
        {
            this.On<iOS>().SetSeparatorStyle(SeparatorStyle.FullWidth);
            this.Footer = "";
        }
    }
}