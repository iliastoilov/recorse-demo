﻿using System;
using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public class CustomFrame : Frame
    {
        public CustomFrame(View content)
        {
            Content = content;
            CornerRadius = 5;
            HasShadow = true;
            BackgroundColor = Colors.MainColor;
            BorderColor = Colors.AccentColor;
            Padding = 10;
            Margin = new Thickness(0, 5);
        }
    }
}
