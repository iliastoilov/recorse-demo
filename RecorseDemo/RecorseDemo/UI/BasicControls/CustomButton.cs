﻿using Xamarin.Forms;

namespace RecorseDemo.UI
{
    public class CustomButton : Button
    {
        public void SetCommandBinding(string propertyName)
        {
            this.SetBinding(Button.CommandProperty, propertyName);
        }

        public void SetCommandParameterBinding(string propertyName)
        {
            this.SetBinding(Button.CommandParameterProperty, propertyName);
        }

        public CustomButton(string text = "")
        {
            Text = text;
            TextColor = Color.White;
            BorderWidth = 1;
            BorderColor = Color.White;
            CornerRadius = 5;
            BackgroundColor = Colors.MainColor;
        }
    }
}