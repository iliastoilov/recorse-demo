﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace RecorseDemo.LocalDatabase
{
    public abstract class BaseService<TEntity, TContext> where TEntity : class where TContext : DbContext
    {
        protected TContext GetContext()
        {
            TContext dbContext = Activator.CreateInstance<TContext>();
            dbContext.Database.EnsureCreated();
            dbContext.Database.Migrate();
            return dbContext;
        }

        public async Task<List<TEntity>> GetAllDataAsync()
        {
            try
            {
                using (var context = GetContext())
                {
                    return await context.Set<TEntity>()
                                        .AsNoTracking()
                                        .ToListAsync();
                }
            }
            catch (Exception ex)
            {
                App.Logger.Report(Logger.LogType.Error, nameof(GetAllDataAsync) + " " + typeof(TEntity), ex);
                return null;
            }
        }

        public async Task UpdateAsync(TEntity entity)
        {
            try
            {
                using (var context = GetContext())
                {
                    context.Set<TEntity>().Update(entity);
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                App.Logger.Report(Logger.LogType.Error, nameof(UpdateAsync) + " " + typeof(TEntity), ex);
            }
        }

        public async Task AddAsync(TEntity entity)
        {
            try
            {
                using (var context = GetContext())
                {
                    await context.Set<TEntity>().AddAsync(entity);
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                App.Logger.Report(Logger.LogType.Error, nameof(AddAsync) + " " + typeof(TEntity), ex);
            }
        }

        public async Task RemoveAsync(TEntity entity)
        {
            try
            {
                using (var context = GetContext())
                {
                    context.Set<TEntity>().Remove(entity);
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                App.Logger.Report(Logger.LogType.Error, nameof(AddAsync) + " " + typeof(TEntity), ex);
            }
        }
    }
}
