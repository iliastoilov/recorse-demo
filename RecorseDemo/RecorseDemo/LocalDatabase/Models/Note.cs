﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RecorseDemo.LocalDatabase.Models
{
    public class Note
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ID { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}
