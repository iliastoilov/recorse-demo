﻿using RecorseDemo.iOS.Renderers;
using RecorseDemo.UI;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(CustomDatePicker), typeof(CustomDatePickerRenderer))]
namespace RecorseDemo.iOS.Renderers
{
    public class CustomDatePickerRenderer : DatePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                Control.Layer.BackgroundColor = Colors.ControlsBackgroundColor.ToCGColor();
                Control.Layer.CornerRadius = 5;
                Control.Layer.BorderWidth = 1;
                Control.Layer.BorderColor = Colors.AccentColor.ToCGColor();
            }
        }
    }
}