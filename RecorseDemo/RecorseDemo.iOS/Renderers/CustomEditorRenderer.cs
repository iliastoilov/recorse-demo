﻿using RecorseDemo.iOS.Renderers;
using RecorseDemo.UI;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace RecorseDemo.iOS.Renderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                Control.Layer.BackgroundColor = Colors.ControlsBackgroundColor.ToCGColor();
                Control.Layer.CornerRadius = 5;
                Control.Layer.BorderWidth = 1;
                Control.Layer.BorderColor = Colors.AccentColor.ToCGColor();
            }
        }
    }
}